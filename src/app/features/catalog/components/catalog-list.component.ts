import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Product } from '../../../model/product';

@Component({
  selector: 'ava-catalog-list',
  template: `

    <div class="row">
      <div class="col-6" *ngFor="let product of products">
        <div class="card mb-5 ">
          <div class="card-header">
            {{product.title}}
            <div class="pull-right"> {{product.price}} </div>
          </div>
          <div class="card-body">
            <img [src]="product.image" alt="" width="50">
            <div class="btn btn-primary" (click)="addToCart.emit(product)">ADD TO CART</div>
          </div>
        </div>
      </div>
    </div>
  `,
})
export class CatalogListComponent {
  @Input() products: Product[] | null = null
  @Output() addToCart = new EventEmitter<Product>();
}
