import { Component } from '@angular/core';
import { CartService } from '../../core/cart.service';
import { CatalogService } from './services/catalog.service';


@Component({
  selector: 'ava-catalog',
  template: `
    <ava-panel></ava-panel>
    <h1 *ngIf="catalogService.products && catalogService.products.length < 10; else error">
      Pochi
    </h1>
    
    <ng-template #error>
      <h1>MOLTIIIIII</h1>
    </ng-template>
    
    <div *ngIf="!catalogService.products">loading....</div>
    
    <ava-catalog-list
      [products]="catalogService.products"
      (addToCart)="cartService.addToCart($event)"
    ></ava-catalog-list>
    
    <!--<h1>{{catalogService.products?.length! > 10 ? ' ci sono tanti prodottti' : 'pochi'}}</h1>-->
    
  `,
})
export class CatalogComponent {
  constructor(
    public catalogService: CatalogService,
    public cartService: CartService
  ) {
    catalogService.getAllProducts()
  }

  getValue() {
    return (this.catalogService.products && this.catalogService.products.length > 10) ? 'molti' : 'pochi'
  }
}
