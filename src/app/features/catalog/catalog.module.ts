import { NgModule } from '@angular/core';
import { CatalogComponent } from './catalog.component';
import { CatalogListComponent } from './components/catalog-list.component';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import { CatalogRoutingModule } from './catalog-routing.module';

@NgModule({
  declarations: [
    CatalogComponent,
    CatalogListComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    CatalogRoutingModule
  ]
})
export class CatalogModule {}
