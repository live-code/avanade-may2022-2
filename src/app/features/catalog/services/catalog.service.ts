import { Injectable } from '@angular/core';
import { Product } from '../../../model/product';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CatalogService {
  products: Product[] | null = null;

  constructor(private http: HttpClient) {}

  getAllProducts() {
    this.http.get<Product[]>('https://fakestoreapi.com/products')
      .subscribe(res => {
        this.products = res;
      })

  }
}
