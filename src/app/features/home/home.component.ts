import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { delay } from 'rxjs';


@Component({
  selector: 'ava-home',
  template: `
    <form #f="ngForm" (submit)="save(f)">
      <input type="text" #inputName="ngModel" [ngModel]="user?.name" name="name" required minlength="3" class="form-control"
             [ngClass]="{'is-invalid': inputName.invalid && f.dirty, 'is-valid': inputName.valid}"
             placeholder="name *">
      
      <input type="text" #inputSurname="ngModel" [ngModel]="user?.email" name="email" required 
             class="form-control"
             [ngClass]="{'is-invalid': inputSurname.invalid && f.dirty, 'is-valid': inputSurname.valid}"
             placeholder="email *">
      
      <input type="checkbox" [ngModel]="user?.subscription" name="subscription"> subscribe newsletter
      
      <select class="form-control" [ngModel]="user?.gender" name="gender" required
              #inputGender="ngModel"
              [ngClass]="{'is-invalid': inputGender.invalid && f.dirty, 'is-valid': inputGender.valid}">
        <option [ngValue]="null">select gender</option>
        <option value="M">Male</option>
        <option value="F">Female</option>
      </select>
      
      <hr>
      <button [disabled]="f.invalid" >save</button>
    </form>
  `,
})
export class HomeComponent  {
  user: any ;

  constructor(private http: HttpClient) {
    http.get('https://jsonplaceholder.typicode.com/users/1')
      .pipe(delay(1000))
      .subscribe(res => {
        this.user = res;
        this.user.subscription = true;
        this.user.gender = 'M'
      })
  }

  save(form: NgForm) {
    console.log(form.value)
    this.http.patch('https://jsonplaceholder.typicode.com/users/1', form.value)
      .subscribe(res => {
        // console.log(res)
      })
  }
}
