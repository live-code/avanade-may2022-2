import { Component, OnInit } from '@angular/core';
import { CartService } from '../../core/cart.service';

@Component({
  selector: 'ava-cart',
  template: `
    <ava-panel></ava-panel>
    <ng-template #noProductsMsg>
      non ci sono prodotti nel carrello
    </ng-template>
    
    <ul  class="alert alert-danger"
         *ngIf="cartService.items.length; else noProductsMsg">
      <h1>LISTA CARRELLO</h1>
      <li *ngFor="let item of cartService.items">
        {{item.title}}
        <button (click)="cartService.deleteFromCart(item.id)">delete</button>
      </li>
    </ul>
  `,
  styles: [
  ]
})
export class CartComponent implements OnInit {

  constructor(public cartService: CartService) { }

  ngOnInit(): void {
  }

}
