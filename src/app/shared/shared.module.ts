import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PanelComponent } from './components/panel.component';
import { TabbarComponent } from './components/tabbar.component';
import { HelloComponent } from './components/hello.component';


@NgModule({
  declarations: [
    PanelComponent,
    TabbarComponent,
    HelloComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    PanelComponent,
    TabbarComponent,
    HelloComponent
  ]
})
export class SharedModule { }
