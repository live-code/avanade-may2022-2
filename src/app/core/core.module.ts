import { NgModule } from '@angular/core';
import { NavbarComponent } from './components/navbar.component';
import { PippoComponent } from './components/pippo.component';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    NavbarComponent,
    PippoComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
  ],
  exports: [
    NavbarComponent,
    // PippoComponent
  ]
})
export class CoreModule { }
