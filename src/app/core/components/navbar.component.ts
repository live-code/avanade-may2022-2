import { Component, OnInit } from '@angular/core';
import { CartService } from '../cart.service';

@Component({
  selector: 'ava-navbar',
  template: `
    <button [routerLink]="'home'">home</button>
    <button routerLink="catalog">catalog</button>
    <button routerLink="cart">cart</button>
    <button routerLink="login">login</button>
    <button routerLink="contacts">contacts</button>
    {{cartService.items.length}} items
    <hr>
    
  `,
})
export class NavbarComponent implements OnInit {
  constructor( public cartService: CartService) {
    console.log(cartService.items)
  }

  ngOnInit(): void {
  }

}
