import { Product } from '../model/product';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  items: Product[] = []

  addToCart(p: Product) {
    this.items.push(p);
  }

  deleteFromCart(id: number) {
    const index = this.items.findIndex(item => item.id === id);
    this.items.splice(index, 1)
  }
}
